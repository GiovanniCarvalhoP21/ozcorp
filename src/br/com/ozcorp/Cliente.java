package br.com.ozcorp;

public class Cliente {
	
	private String nome;
	private String cpf;
	private String rg;
	private String Matricula;
	private String Email;
	private String Sexo;
	private String Cargo;
	private String Salario;
	private String Senha;
	private String Sangue;
	
	public Cliente(String nome, String cpf, String rg, String Matricula, String Email, String Sexo, String Cargo, String Salario, String Senha, String Sangue) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.Matricula = Matricula;
		this.Email = Email;
		this.Sexo = Sexo;
		this.Cargo = Cargo;
		this.Salario = Salario;
		this.Senha = Senha;
		this.Sangue = Sangue;
	}
	
	//
	
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	public String getRg() {
		return rg;
	}
	public String getMatricula() {
		return Matricula;
	}
	public String getEmail() {
		return Email;
	}
	public String getSexo() {
		return Sexo;
	}
	public String getCargo() {
		return Cargo;
	}
	public String getSalario() {
		return Salario;
	}
	public String getSenha() {
		return Senha;
	}
	public String getSangue() {
		return Sangue;
	}
}