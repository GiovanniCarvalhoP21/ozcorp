package br.com.ozcorp;

public class FuncionarioTest {
	
public static void main(String[] args) {
		
		System.out.println("*****************************************************************************************0");
		Cliente carlos = new Cliente("carlos", "123.456.789-10", "33.433.123-9", "7529163", "CarlosRobertoPinto@gmail.com", "masculino","o-Diretor","10000.00", "123", "AB+");
		System.out.println("nome: " + carlos.getNome());
		System.out.println("CPF: " + carlos.getCpf());
		System.out.println("RG: " + carlos.getRg());
		System.out.println("Matricula: " + carlos.getMatricula());
		System.out.println("Email: " + carlos.getEmail());
		System.out.println("Sexo: " + carlos.getSexo());
		System.out.println("Nivel-Cargo: " + carlos.getCargo());
		System.out.println("Salario : " + carlos.getSalario());
		System.out.println("Senha : " + carlos.getSenha());
		System.out.println("Sangue : " + carlos.getSangue());
		System.out.println("*****************************************************************************************1");
		Cliente roberta = new Cliente("roberta", "427.356.749-91", "31.223.193-1", "4922111", "RobertaCosta@Hotmail.com","Feminino","1-Secretaria","8000.00", "312", "O+");
		System.out.println("nome: " + roberta.getNome());
		System.out.println("CPF: " + roberta.getCpf());
		System.out.println("RG: " + roberta.getRg());
		System.out.println("Matricula: " + roberta.getMatricula());
		System.out.println("Email: " + roberta.getEmail());
		System.out.println("Sexo: " + roberta.getSexo());
		System.out.println("Nivel-Cargo: " + roberta.getCargo());
		System.out.println("Salario : " + roberta.getSalario());
		System.out.println("Senha : " + roberta.getSenha());
		System.out.println("Sangue : " + roberta.getSangue());
		System.out.println("*****************************************************************************************2");
		Cliente TamiTsunami = new Cliente("Tamires Markes", "127.216.341-62", "11.322.123-3", "1325121", "POWERLGBT@Hotmail.com","Indefinido","2-Gerente","6000.00", "624", "B-");
		System.out.println("nome: " + TamiTsunami.getNome());
		System.out.println("CPF: " + TamiTsunami.getCpf());
		System.out.println("RG: " + TamiTsunami.getRg());
		System.out.println("Matricula: " + TamiTsunami.getMatricula());
		System.out.println("Email: " + TamiTsunami.getEmail());
		System.out.println("Sexo: " + TamiTsunami.getSexo());
		System.out.println("Nivel-Cargo: " + TamiTsunami.getCargo());
		System.out.println("Salario : " + TamiTsunami.getSalario());
		System.out.println("Senha : " + TamiTsunami.getSenha());
		System.out.println("Sangue : " + TamiTsunami.getSangue());
		System.out.println("*****************************************************************************************3");
		Cliente claudio = new Cliente("Claudio", "327.212.120-02", "61.121.653-0", "7254121", "ClaudioODestruidor@Gmail.com","Masculino","3-Engenhero","4000.00", "291", "B+");
		System.out.println("nome: " + claudio.getNome());
		System.out.println("CPF: " + claudio.getCpf());
		System.out.println("RG: " + claudio.getRg());
		System.out.println("Matricula: " + claudio.getMatricula());
		System.out.println("Email: " + claudio.getEmail());
		System.out.println("Sexo: " + claudio.getSexo());
		System.out.println("Nivel-Cargo: " + claudio.getCargo());
		System.out.println("Salario : " + claudio.getSalario());
		System.out.println("Senha : " + claudio.getSenha());
		System.out.println("Sangue : " + claudio.getSangue());
		System.out.println("*****************************************************************************************4");
		Cliente Lydia = new Cliente("Lydia", "121.111.121-12", "21.122.252-2", "8732121", "Lydia@Gmail.com","Feminino","4-Analista","2000.00", "111", "O-");
		System.out.println("nome: " + Lydia.getNome());
		System.out.println("CPF: " + Lydia.getCpf());
		System.out.println("RG: " + Lydia.getRg());
		System.out.println("Matricula: " + Lydia.getMatricula());
		System.out.println("Email: " + Lydia.getEmail());
		System.out.println("Sexo: " + Lydia.getSexo());
		System.out.println("Nivel-Cargo: " + Lydia.getCargo());
		System.out.println("Salario : " + Lydia.getSalario());
		System.out.println("Senha : " + Lydia.getSenha());
		System.out.println("Sangue : " + Lydia.getSangue());
		System.out.println("*****************************************************************************************5");
		
	}

}

